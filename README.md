# Dotfiles - Chezmoi

> Yet another `chezmoi` managed _doftiles_ repository

This repository is mainly a convenience to gather the actual configurations listed in `./.chezmoiexternal.toml` 🧐

I don't feel confortable with managing all my configurations in a single repository as I sometimes need to pick one or two on some remote machines.

So, yeah, not much to see here actually 🤷
But for what it's worth, I used to have my _doftiles_ managed with `git` submodules and `stow`. 
This is much simpler and allows me to have `stow`-agnostic configurations.
Which is nice 👍

## Usage 

```bash
chezmoi init --apply git@gitlab.com:pcoves/dotfiles.git
```
